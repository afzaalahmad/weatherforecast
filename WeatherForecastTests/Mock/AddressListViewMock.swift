//
//  AddressListViewMock.swift
//  WeatherForecastTests
//
//  Created by Afzaal Ahmad on 6/10/18.
//  Copyright © 2018 Afzaal Ahmad. All rights reserved.
//

@testable import WeatherForecast

class AddressListViewMock: SearchAddressView {
    
    var setLoadingCalled = false
    var setFinishCalled = false
    var setAddressesCalled = false
    var setEmptyAddressesCalled = false
    
    func startLoading() {
        setLoadingCalled = true
    }
    
    func finishLoading() {
        setFinishCalled = true
    }
    
    func setAddresss(addresses: [Address]) {
        setAddressesCalled = true
    }
    
    func setEmptyAddresses() {
        setEmptyAddressesCalled = true
    }
    
    
}
