//
//  AddressServiceMock.swift
//  WeatherForecastTests
//
//  Created by Afzaal Ahmad on 6/10/18.
//  Copyright © 2018 Afzaal Ahmad. All rights reserved.
//
@testable import WeatherForecast

class AddressServiceMock: AddressService {
    private let addresses: [Address]
    
    init(addresses: [Address]) {
        self.addresses = addresses
    }
    
    func getAddresses(searchFor: String, completion: @escaping (Result<AddressResult?, APIError>) -> Void) {
        let result = Result<AddressResult?, APIError>.success(AddressResult(results: addresses, status: "OK"))
        
        completion(result)
    }

}
