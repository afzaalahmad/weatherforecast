//
//  AddressTests.swift
//  WeatherForecastTests
//
//  Created by Afzaal Ahmad on 6/10/18.
//  Copyright © 2018 Afzaal Ahmad. All rights reserved.
//

import XCTest
@testable import WeatherForecast

class AddressTests: XCTestCase {
    var address: Address!

    override func setUp() {
        address = Address(placeId: "abc123", formattedAddress: "Long Island City, NY", location: Location(lat: 40.792240, lng: -73.138260))
    }
    
    override func tearDown() {
        address = nil
    }
    
    func testAddress() {
        //verify
        XCTAssertTrue(
            address.placeId == "abc123" &&
            address.formattedAddress == "Long Island City, NY"
        )
    }
    
    func testLocation() {
        //verify
        XCTAssertTrue(
            address.location.lat == 40.792240 &&
            address.location.lng == -73.138260
        )
    }
    
}
