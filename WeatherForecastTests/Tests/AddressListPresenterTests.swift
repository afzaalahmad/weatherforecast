//
//  AddressListPresenterTests.swift
//  WeatherForecastTests
//
//  Created by Afzaal Ahmad on 6/10/18.
//  Copyright © 2018 Afzaal Ahmad. All rights reserved.
//
import XCTest
@testable import WeatherForecast

class AddressListPresenterTests: XCTestCase {

    var emptyAddressService: AddressService!
    var twoAddressService: AddressService!

    override func setUp() {
        emptyAddressService = AddressServiceMock(addresses: [])
        
        let addresses = [
            Address(placeId: "abc123", formattedAddress: "Long Island City, NY", location: Location(lat: 40.792240, lng: -73.138260)),
            Address(placeId: "abc124", formattedAddress: "Manhattan, NY", location: Location(lat: 40.7590403, lng: -74.0392709))
                         ]
        twoAddressService = AddressServiceMock(addresses: addresses)
    }
    
    override func tearDown() {
        emptyAddressService = nil
        twoAddressService = nil
    }

    func testShouldSetAddresses() {
        //given
        let addressListViewMock = AddressListViewMock()
        
        let searchAdressPresenterUnderTest = SearchAdressPresenter(addressService: twoAddressService)
        searchAdressPresenterUnderTest.attachView(view: addressListViewMock)
        
        //when
        searchAdressPresenterUnderTest.getAddresses(searchFor: "test")
        
        //verify
        XCTAssertTrue(addressListViewMock.setAddressesCalled)
    }
    
    func testShouldSetEmpty() {
        //given
        let addressListViewMock = AddressListViewMock()
        
        let searchAdressPresenterUnderTest = SearchAdressPresenter(addressService: emptyAddressService)
        searchAdressPresenterUnderTest.attachView(view: addressListViewMock)
        
        //when
        searchAdressPresenterUnderTest.getAddresses(searchFor: "test")
        
        //verify
        XCTAssertTrue(addressListViewMock.setEmptyAddressesCalled)
    }
}
