# Main architecture
### MVP: Model, View, Presenter
![Alt text](http://iyadagha.com/wp-content/uploads/2016/02/Bildschirmfoto-2016-02-01-um-22.23.57.png "MVP Diagrame")

* the view part of the MVP consists of both UIViews and UIViewController

* the view delegates user interactions to the presenter

* the presenter contains the logic to handle user interactions

* the presenter communicates with model layer, converts the data to UI friendly format, and updates the view


### WHY: 
* the presenter has no dependencies to UIKit

* the view is passiv (dump)

* test application layers independently

* we don’t need to write unit tests for the UIViewController anymore.


# Improvements
* Core Data layer need improvements

* Google Maps API not confiure

* Controllers design

# Not Delivered
* all unit tests are not implemented
