//
//  WeatherListPresenter.swift
//  WeatherForecast
//
//  Created by Afzaal Ahmad on 6/10/18.
//  Copyright © 2018 Afzaal Ahmad. All rights reserved.
//

import Foundation

class WeatherListPresenter {
    private let weatherListService: WeatherListService
    weak private var weatherListView: WeatherListView?
    private var locationMmanager: OneShotLocationManager?
    
    var orderAscending: Bool = false
    
    init(weatherListService: WeatherListService) {
        self.weatherListService = weatherListService
    }
    
    func attachView(view: WeatherListView) {
        self.weatherListView = view
    }
    
    func loadDaysWeather(predicate: NSPredicate?) {
        weatherListService.getDaysWeather(predicate: predicate, orderAscending: orderAscending) { (daysWeather) in
            let arrCDDayWeatherData = daysWeather.map({ (dayWeather) -> CDDayWeatherData in
                return CDDayWeatherData(dayWeather: dayWeather)
            })
            self.weatherListView?.setDayWeaters(dayWeathers: arrCDDayWeatherData)
        }
    }
    
    func syncData() {
        
        guard let defaultLocation = CDLocation.getDefaultLocation() else{
            return
        }
        
        if(defaultLocation.isUserLocation()){
            locationMmanager = OneShotLocationManager()
            locationMmanager?.fetchWithCompletion {[weak self] (location, error) in
                self?.locationMmanager = nil
                DispatchQueue.main.async {
                    if let location = location{
                        ///Clear cache if user location is changed
                        if let dayWeathers = defaultLocation.dayWeathers, dayWeathers.count > 0{
                            
                            if(defaultLocation.latitude.rounded(toPlaces: 6) != location.coordinate.latitude.rounded(toPlaces: 6) ||
                                defaultLocation.longitude.rounded(toPlaces: 6) != location.coordinate.longitude.rounded(toPlaces: 6)
                                ){
                                defaultLocation.removeFromDayWeathers(dayWeathers)
                            }
                        }
                        
                        defaultLocation.latitude = location.coordinate.latitude
                        defaultLocation.longitude = location.coordinate.longitude
                        CoreDataManager.shared.saveContext()
                        
                        self?.loadLocationData(cdLocation: defaultLocation)
                    }
                    else{
                        self?.weatherListView?.dataSynced(error: error)
                    }
                }
            }
        }
        else{
            self.loadLocationData(cdLocation: defaultLocation)
        }
    }
    
    private func loadLocationData(cdLocation: CDLocation) {
        WeatherService().getWeatherDataFromServer(location: cdLocation) { [weak self] (result) in
            
            switch result {
            case .success(let data):
                if let data = data, let daily = data["daily"] as? [String : Any], let dayWeathers = daily["data"] as? [[String : Any]]{
                    self?.weatherListService.saveDayWeathers(location: cdLocation, dayWeathers: dayWeathers, completion: {
                        DispatchQueue.main.async {
                            self?.weatherListView?.dataSynced(error: nil)
                        }
                    })
                }
            case .failure( _):
                self?.weatherListView?.dataSynced(error: nil)
            }
        }
    }
    
}
