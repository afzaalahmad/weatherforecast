//
//  AddressListPresenter.swift
//  WeatherForecast
//
//  Created by Afzaal Ahmad on 6/9/18.
//  Copyright © 2018 Afzaal Ahmad. All rights reserved.
//

class SearchAdressPresenter {
    private let addressService: AddressService
    weak private var addressListView : SearchAddressView?
    
    init(addressService: AddressService){
        self.addressService = addressService
    }
    
    func attachView(view: SearchAddressView){
        addressListView = view
    }
    
    func detachView() {
        addressListView = nil
    }
    
    func getAddresses(searchFor: String){
        self.addressListView?.startLoading()
        
        addressService.getAddresses(searchFor: searchFor) { [weak self] (result) in
            
            self?.addressListView?.finishLoading()
            
            switch result {
            case .success(let addressResult):
                
                guard let addressResult = addressResult, let addresses = addressResult.results else {
                    self?.addressListView?.setEmptyAddresses()
                    return
                }
                
                if(addresses.count == 0){
                    self?.addressListView?.setEmptyAddresses()
                }else{
                    self?.addressListView?.setAddresss(addresses: addresses)
                }
                
            case .failure( _):
                self?.addressListView?.setEmptyAddresses()
            }
        }
    }
}
