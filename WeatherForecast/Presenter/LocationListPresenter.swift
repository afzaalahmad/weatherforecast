//
//  AddressListPresenter.swift
//  WeatherForecast
//
//  Created by Afzaal Ahmad on 6/9/18.
//  Copyright © 2018 Afzaal Ahmad. All rights reserved.
//
import Foundation

protocol DefaultLocation : AnyObject{
    func setDefaultLocation(cdLocation: CDLocation)
    func getDefaultLocation() -> CDLocation?
}

class LocationListPresenter {
    private let locationService: LocationService
    weak private var locationListView : LocationListView?
    
    init(locationService: LocationService){
        self.locationService = locationService
    }
    
    func attachView(view: LocationListView){
        locationListView = view
    }
    
    func detachView() {
        locationListView = nil
    }
    
    func addLocation(address: Address){
        self.locationService.addLocation(address: address)
        self.refreshListView()
    }
    
    func getAddresses(){
        self.refreshListView()
    }
    
    private func refreshListView() {
        self.locationService.getLocations(predicate: nil, orderAscending:  true) { (locations) in
            if(locations.count == 0){
                self.locationListView?.setEmptyLocations()
            }
            else{
                self.locationListView?.setLocations(locations: locations)
            }
        }
    }
}

extension LocationListPresenter: DefaultLocation{
    func setDefaultLocation(cdLocation: CDLocation){
        cdLocation.setAsDefaultLocation()
        guard let  defaultLocation = CDLocation.getDefaultLocation() else {
            return
        }
        
        self.locationListView?.setDefaultLocation(cdLocation: defaultLocation)
        
    }
    func getDefaultLocation() -> CDLocation?{
        return CDLocation.getDefaultLocation()
    }
}


