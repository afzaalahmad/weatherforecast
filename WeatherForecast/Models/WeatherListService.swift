//
//  WeatherListService.swift
//  WeatherForecast
//
//  Created by Afzaal Ahmad on 6/11/18.
//  Copyright © 2018 Afzaal Ahmad. All rights reserved.
//

import Foundation
import CoreData

protocol WeatherListService {
    func getDaysWeather(predicate: NSPredicate?, orderAscending: Bool, callBack:([CDDayWeather]) -> Void)
    func saveDayWeathers(location: CDLocation, dayWeathers: [[String : Any]]?, completion: @escaping (()-> Void))
}

class WeatherListServiceCoreData: WeatherListService {
    func addDayWeather(dayWeatherDic: [String : Any], context: NSManagedObjectContext) -> CDDayWeather?{
        if let time = dayWeatherDic["time"] as? TimeInterval{
            
            let date = Date(timeIntervalSince1970: time)
            let dayWeather = getOrCreateDayWeather(date: date, context: context)
            
            let summary = dayWeatherDic["summary"] as? String
            let icon = dayWeatherDic["icon"] as? String
            
            dayWeather.summary = summary
            dayWeather.icon = icon
            
            if let sunRiseTime = dayWeatherDic["sunriseTime"] as? TimeInterval{
                dayWeather.sunriseTime = Date(timeIntervalSince1970: sunRiseTime)
            }
            if let sunSetTime = dayWeatherDic["sunsetTime"] as? TimeInterval{
                dayWeather.sunsetTime = Date(timeIntervalSince1970: sunSetTime)
            }
            
            if let temperatureLow = dayWeatherDic["temperatureLow"] as? Double{
                dayWeather.temperatureLow = Float(temperatureLow)
            }
            
            if let temperatureHigh = dayWeatherDic["temperatureHigh"] as? Double{
                dayWeather.temperatureHigh = Float(temperatureHigh)
            }
            
            return dayWeather
        }
        
        return nil
    }
    
    func getOrCreateDayWeather(date: Date, context: NSManagedObjectContext) -> CDDayWeather {
        if let dayWeather = getDayWeather(date: date, context: context){
            return dayWeather
        }
        
        let dayWeather = CDDayWeather(context: context)
        dayWeather.time = date
        
        return dayWeather
    }
    
    func getDayWeather(date: Date, context: NSManagedObjectContext) -> CDDayWeather?{
        do {
            let fetchRequest: NSFetchRequest<CDDayWeather> = CDDayWeather.fetchRequest()
            fetchRequest.predicate = NSPredicate(format: "time == %@", date as CVarArg)
            fetchRequest.fetchLimit = 1
            
            return try context.fetch(fetchRequest).first
        }
        catch {
            return nil
        }
    }
    
    func saveDayWeathers(location: CDLocation, dayWeathers: [[String : Any]]?, completion: @escaping (()-> Void)) {
        guard let dayWeathers = dayWeathers, dayWeathers.count > 0 else{
            completion()
            return
        }
        
        CoreDataManager.shared.persistentContainer.performBackgroundTask { (backgroundContext) in
            let location = backgroundContext.object(with: location.objectID) as! CDLocation
            for dayWeatherDic in dayWeathers{
                let dayWeather = self.addDayWeather(dayWeatherDic: dayWeatherDic, context: backgroundContext)
                dayWeather?.location = location
            }
            try! backgroundContext.save()
            
            completion()
        }
    }
    
    func getDaysWeather(predicate: NSPredicate?, orderAscending: Bool, callBack:([CDDayWeather]) -> Void) {
        let fetchRequest: NSFetchRequest<CDDayWeather> = CDDayWeather.fetchRequest()
        fetchRequest.predicate = predicate

        fetchRequest.sortDescriptors =  [NSSortDescriptor(key: "time", ascending: orderAscending)]
        do{
            let arrCDDayWeather = try CoreDataManager.shared.persistentContainer.viewContext.fetch(fetchRequest)
            callBack(arrCDDayWeather)
        }
        catch{
            callBack([])
        }
    }
}
