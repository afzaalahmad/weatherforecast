//
//  LocationCoreDataService.swift
//  WeatherForecast
//
//  Created by Afzaal Ahmad on 6/12/18.
//  Copyright © 2018 Afzaal Ahmad. All rights reserved.
//

import Foundation

import Foundation
import CoreData

protocol LocationService {
    func addLocation(address: Address)
    func getLocation(placeID: String) -> CDLocation?
    func deleteLocation(placeId: String)
    func isSaveLocation(placeId: String) -> Bool
    func getLocations(predicate: NSPredicate?, orderAscending: Bool, callBack:([CDLocation]) -> Void)
}

class LocationServiceCoreData: LocationService{
    func addLocation(address: Address) {
        if(!self.isSaveLocation(placeId: address.placeId)){
            let cdLocation = CDLocation(context: CoreDataManager.shared.persistentContainer.viewContext)
            cdLocation.placeID = address.placeId
            cdLocation.title = address.formattedAddress
            cdLocation.latitude = address.location.lat
            cdLocation.longitude = address.location.lng
            
            CoreDataManager.shared.saveContext()
        }
    }
    
    func getLocations(predicate: NSPredicate?, orderAscending: Bool, callBack: ([CDLocation]) -> Void) {
        let fetchRequest: NSFetchRequest<CDLocation> = CDLocation.fetchRequest()
        fetchRequest.predicate = predicate
        fetchRequest.sortDescriptors =  [NSSortDescriptor(key: "title", ascending: orderAscending)]
        do{
            let arrCDLocation = try CoreDataManager.shared.persistentContainer.viewContext.fetch(fetchRequest)
            callBack(arrCDLocation)
        }
        catch{
            callBack([])
        }
    }
    
    func deleteLocation(placeId: String) {
        if let cdLocation = getLocation(placeID: placeId){
            CoreDataManager.shared.persistentContainer.viewContext.delete(cdLocation)
            CoreDataManager.shared.saveContext()
        }
    }
    
    func isSaveLocation(placeId: String) -> Bool {
        return getLocation(placeID: placeId) != nil
    }
    
    func getLocation(placeID: String) -> CDLocation? {
        do {
            let fetchRequest: NSFetchRequest<CDLocation> = CDLocation.fetchRequest()
            fetchRequest.predicate = NSPredicate(format: "placeID == %@", placeID)
            fetchRequest.fetchLimit = 1
            
            return try CoreDataManager.shared.persistentContainer.viewContext.fetch(fetchRequest).first
        }
        catch {
            return nil
        }
    }
}
