//
//  Address.swift
//  WeatherForecast
//
//  Created by Afzaal Ahmad on 6/9/18.
//  Copyright © 2018 Afzaal Ahmad. All rights reserved.
//

struct Address {
    let placeId: String
    let formattedAddress: String
    let location: Location
    
    enum CodingKeys : String, CodingKey {
        case placeId = "place_id"
        case formattedAddress = "formatted_address"
        case geometry = "geometry"
    }
    
    enum GeometryKeys : String, CodingKey {
        case location = "location"
    }
}

extension Address: Decodable {
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        placeId = try values.decode(String.self, forKey: .placeId)
        formattedAddress = try values.decode(String.self, forKey: .formattedAddress)
        
        let geometry = try values.nestedContainer(keyedBy: GeometryKeys.self, forKey: .geometry)
        location = try geometry.decode(Location.self, forKey: .location)
    }
}

extension Address: Encodable {
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(placeId, forKey: .placeId)
        try container.encode(formattedAddress, forKey: .formattedAddress)
        var geometry = container.nestedContainer(keyedBy: GeometryKeys.self, forKey: .geometry)
        try geometry.encode(location, forKey: .location)
    }
}

struct Location: Codable {
    let lat: Double
    let lng: Double
}

struct AddressResult: Codable {
    let results: [Address]?
    let status: String
    
}
