//
//  AddressListView.swift
//  WeatherForecast
//
//  Created by Afzaal Ahmad on 6/9/18.
//  Copyright © 2018 Afzaal Ahmad. All rights reserved.
//

protocol SearchAddressView: AnyObject {
    func startLoading()
    func finishLoading()
    func setAddresss(addresses: [Address])
    func setEmptyAddresses()
}
