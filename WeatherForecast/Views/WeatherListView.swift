//
//  WeatherListView.swift
//  WeatherForecast
//
//  Created by Afzaal Ahmad on 6/10/18.
//  Copyright © 2018 Afzaal Ahmad. All rights reserved.
//

import Foundation

protocol WeatherListView: AnyObject {
    func setDayWeaters(dayWeathers: [CDDayWeatherData])
    
    func dataSynced(error: Error?)
}

struct CDDayWeatherData {
    let day: String
    let formatedDate: String
    let summary: String
    let skycons: Skycons
    let sunRiseTime: String
    let sunSetTime: String
    let tempLow: String
    let tempHigh: String
    
    init(dayWeather: CDDayWeather) {
        self.day = dayWeather.time?.dayOfWeek() ?? ""
        self.formatedDate =  self.day + ", " + ((dayWeather.time?.string(formate: "MMM d, yyyy")) ?? "")
        self.summary = dayWeather.summary ?? ""
        self.skycons = Skycons(icon: dayWeather.icon)
        self.sunRiseTime = dayWeather.sunriseTime?.time().uppercased() ?? ""
        self.sunSetTime = dayWeather.sunsetTime?.time().uppercased() ?? ""
        self.tempLow = "\(dayWeather.temperatureLow)"
        self.tempHigh = "\(dayWeather.temperatureHigh)"
    }
}
