//
//  UIViewController+Extention.swift
//  WeatherForecast
//
//  Created by Afzaal Ahmad on 6/13/18.
//  Copyright © 2018 Afzaal Ahmad. All rights reserved.
//

import UIKit

extension UIViewController{
    func showAlert(title: String, message: String, handler: ((UIAlertAction) -> Swift.Void)?) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        let okAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
        alertController.addAction(okAction)
        
        
        self.present(alertController, animated: true, completion: nil)
    }
}
