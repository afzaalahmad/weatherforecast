//
//  CDLocation+Extention.swift
//  WeatherForecast
//
//  Created by Afzaal Ahmad on 6/12/18.
//  Copyright © 2018 Afzaal Ahmad. All rights reserved.
//

import Foundation

extension CDLocation{
    func setAsDefaultLocation() {
        UserDefaults.standard.set(self.placeID , forKey: "default-location-place-id")
        UserDefaults.standard.synchronize()
    }
    
    func isUserLocation() -> Bool {
        guard let location = CDLocation.getDefaultLocation() else{
            return false
        }
        
        return location.placeID == "user-location"
    }
    static func getDefaultLocation() -> CDLocation? {
        guard let defaultLocationPlaceID = UserDefaults.standard.value(forKey: "default-location-place-id") as? String else{ return nil }
        
        let locationService = LocationServiceCoreData()
        
        return locationService.getLocation(placeID: defaultLocationPlaceID)
    }
}
