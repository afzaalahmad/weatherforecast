//
//  Date+Extension.swift
//  WeatherForecast
//
//  Created by Afzaal Ahmad on 6/11/18.
//  Copyright © 2018 Afzaal Ahmad. All rights reserved.
//

import Foundation

extension Date {
    func dayOfWeek() -> String {
        if(NSCalendar.current.isDateInToday(self)){
            return "TODAY"
        }
        else{
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "EEEE"
            return dateFormatter.string(from: self).capitalized
        }
    }
    
    func time() -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "h:mm a"
        return dateFormatter.string(from: self).capitalized
    }
    
    func dateAtMidnight() -> Date {
        var calendar = NSCalendar.current
        calendar.timeZone = NSTimeZone.local
        
        return calendar.startOfDay(for: Date())
    }
    
    func string(formate: String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = formate
        return dateFormatter.string(from: self)
    }
}
