//
//  WeatherService.swift
//  WeatherForecast
//
//  Created by Afzaal Ahmad on 6/13/18.
//  Copyright © 2018 Afzaal Ahmad. All rights reserved.
//

import Foundation

class WeatherService: APIClient{
    
    var session: URLSession
    
    private var task: URLSessionDataTask?
    
    init(configuration: URLSessionConfiguration) {
        self.session = URLSession(configuration: configuration)
    }
    
    convenience init() {
        self.init(configuration: .default)
    }
    
    func getWeatherDataFromServer(location: CDLocation, completion: @escaping (Result<[String: Any]?, APIError>) -> Void){
        ///Cancel previous network request for address api
        self.task?.cancel()
        
        guard let url = URL(string: "https://api.darksky.net/forecast/5315c5dca1447d8350b6d39f4fcda7f7/\(location.latitude),\(location.longitude)?exclude=currently,minutely,hourly,flags") else{
            return
        }
        
        let request = URLRequest(url: url)
        
        self.task = fetch(request: request, completion: completion)
    }
}
