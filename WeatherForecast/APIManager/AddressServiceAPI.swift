//
//  AddressService.swift
//  WeatherForecast
//
//  Created by Afzaal Ahmad on 6/9/18.
//  Copyright © 2018 Afzaal Ahmad. All rights reserved.
//
import Foundation

protocol AddressService {
    func getAddresses(searchFor: String, completion: @escaping (Result<AddressResult?, APIError>) -> Void)
}

class AddressServiceAPI: AddressService, APIClient{
    var session: URLSession
    
    private var task: URLSessionDataTask?
    
    init(configuration: URLSessionConfiguration) {
        self.session = URLSession(configuration: configuration)
    }
    
    convenience init() {
        self.init(configuration: .default)
    }
    
    func getAddresses(searchFor: String, completion: @escaping (Result<AddressResult?, APIError>) -> Void) {
        
        ///Cancel previous network request for address api
        self.task?.cancel()
        
        guard let url = URL(string: "https://maps.googleapis.com/maps/api/geocode/json?address=\(searchFor)&sensor=false") else{
            return
        }
        
        let request = URLRequest(url: url)
        
        self.task = fetch(with: request , decode: { json -> AddressResult? in
            guard let addressResult = json as? AddressResult else { return  nil }
            return addressResult
        }, completion: completion)
    }
}
