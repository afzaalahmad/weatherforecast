//
//  AppDelegate.swift
//  WeatherForecast
//
//  Created by Afzaal Ahmad on 6/4/18.
//  Copyright © 2018 Afzaal Ahmad. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, UISplitViewControllerDelegate {

    var window: UIWindow?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        if(CDLocation.getDefaultLocation() == nil){
            self.createUserLocationForForcast()
        }
        
        self.configureRootView()
        return true
    }
    
    func configureRootView() {
        guard let navigationC = self.window?.rootViewController as? UINavigationController, let weatherVC = navigationC.viewControllers.first as? WeatherViewController else{
            return
        }
        //Set forecast weather configration
        weatherVC.weatherPredicate = NSPredicate(format: "time >= %@", Date().dateAtMidnight() as CVarArg)
        weatherVC.listOrderAscending = true
    }
    
    func createUserLocationForForcast() {
        let location = CDLocation(context: CoreDataManager.shared.persistentContainer.viewContext)
        location.placeID = "user-location"
        location.title = "User Location"
        location.order = 0
        
        location.setAsDefaultLocation()
    
        CoreDataManager.shared.saveContext()
    }
}

