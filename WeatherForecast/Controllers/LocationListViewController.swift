//
//  AddressListViewController.swift
//  WeatherForecast
//
//  Created by Afzaal Ahmad on 6/9/18.
//  Copyright © 2018 Afzaal Ahmad. All rights reserved.
//

import UIKit

protocol LocationListViewControllerDelegate{
    func didChangedLocation(cdLocation: CDLocation, sender: LocationListViewController)
}


class LocationListViewController: UIViewController {

    @IBOutlet weak var emptyView: UIView?
    @IBOutlet weak var tableView: UITableView?
    
    private var defaultLocation: CDLocation?
    
    private let locationListPresenter = LocationListPresenter(locationService: LocationServiceCoreData())
    private var locationsToDisplay = [CDLocation]()
    
    public var delegate: LocationListViewControllerDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.initilizeView()
    }
    
    
    func initilizeView() {
        locationListPresenter.attachView(view: self)
        emptyView?.isHidden = true
        
        self.defaultLocation = locationListPresenter.getDefaultLocation()
        locationListPresenter.getAddresses()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let searchLocationVC = segue.destination as? SearchLocationViewController{
            searchLocationVC.delegate = self
        }
    }
    
}


extension LocationListViewController: LocationListView{
    func setDefaultLocation(cdLocation: CDLocation) {
        self.defaultLocation = cdLocation
        self.tableView?.reloadData()
    }
    
    func getDefaultLocation() -> CDLocation? {
        return self.defaultLocation
    }
    
    
    func setLocations(locations: [CDLocation]){
        locationsToDisplay = locations
        tableView?.isHidden = false
        emptyView?.isHidden = true;
        tableView?.reloadData()
    }
    
    func setEmptyLocations() {
        locationsToDisplay = []
        tableView?.isHidden = true
        emptyView?.isHidden = false;
        tableView?.reloadData()
    }
}


extension LocationListViewController: UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return locationsToDisplay.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Address-cell", for: indexPath)
        let location = self.locationsToDisplay[indexPath.row]
        cell.textLabel?.text = location.title
        cell.accessoryType = (location.placeID == defaultLocation?.placeID) ? .checkmark : .none
        return cell
    }
}

extension LocationListViewController: UITableViewDelegate{
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cdLocation = locationsToDisplay[indexPath.row]
        
        self.locationListPresenter.setDefaultLocation(cdLocation: cdLocation)
        self.delegate?.didChangedLocation(cdLocation: cdLocation, sender: self)
    }
}

extension LocationListViewController: SearchLocationViewControllerDelegate{
    func didSelectAddress(address: Address, sender: SearchLocationViewController) {
        self.locationListPresenter.addLocation(address: address)
        sender.navigationController?.popViewController(animated: true)
    }
}
