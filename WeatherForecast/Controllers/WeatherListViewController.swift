//
//  WeatherListViewController.swift
//  WeatherForecast
//
//  Created by Afzaal Ahmad on 6/10/18.
//  Copyright © 2018 Afzaal Ahmad. All rights reserved.
//

import UIKit

private let reuseIdentifier = "Cell"

class WeatherListViewController: UIViewController {

    @IBOutlet weak var collectionView: UICollectionView!
    
    var weatherListPresenter: WeatherListPresenter?
    
    var arrCDDayWeatherData: [CDDayWeatherData] = []
    
    var weatherPredicate: NSPredicate?
    var listOrderAscending = true
    
    let transition = BounceAnimator()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if(self.weatherListPresenter == nil){
            self.weatherListPresenter = WeatherListPresenter(weatherListService: WeatherListServiceCoreData())
        }
        
        self.weatherListPresenter?.attachView(view: self)
        self.weatherListPresenter?.orderAscending = listOrderAscending
        self.weatherListPresenter?.syncData()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.weatherListPresenter?.loadDaysWeather(predicate: self.predicate)
    }
    
    private var predicate: NSPredicate?{
        get{
            guard let location = CDLocation.getDefaultLocation() else{
                return nil
            }
            
            let locationPredicate = NSPredicate(format: "location == %@", location as CVarArg)
            
            guard let weatherPredicate = self.weatherPredicate else{
                return locationPredicate
            }
            
            return NSCompoundPredicate(andPredicateWithSubpredicates: [locationPredicate, weatherPredicate])
        }
    }

    func syncData() {
        self.weatherListPresenter?.syncData()
    }
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let navigationVC = segue.destination as? UINavigationController{
            if let dayWeatherDetailVC = navigationVC.viewControllers.first as? DayWeatherDetailViewController{
                navigationVC.transitioningDelegate = self
                if let selectedIndexPath = collectionView?.indexPathsForSelectedItems?.first{
                    dayWeatherDetailVC.dayWeatherData = arrCDDayWeatherData[selectedIndexPath.row]
                }
            }
        }
    }
    
    @IBAction func unwindToWeatherListViewController(segue:UIStoryboardSegue) { }
    
}

extension WeatherListViewController: UICollectionViewDataSource{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrCDDayWeatherData.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath)
        configureCell(cell: cell, indexPath: indexPath)
        return cell
    }
    
    func configureCell(cell: UICollectionViewCell, indexPath: IndexPath) {
        let dayWeatherData = arrCDDayWeatherData[indexPath.row]
        
        let iconView = cell.viewWithTag(2) as! SKYIconView
        let temperatureLabel = cell.viewWithTag(3) as! UILabel
        let dayLabel = cell.viewWithTag(4) as! UILabel
        
        dayLabel.text = dayWeatherData.formatedDate
        iconView.setType = dayWeatherData.skycons
        temperatureLabel.text = "\(dayWeatherData.tempLow)\n\(dayWeatherData.tempHigh)"
    }
}

extension WeatherListViewController: WeatherListView{
    func dataSynced(error: Error?) {
        if let error = error{
            self.showAlert(title: "Error", message: error.localizedDescription, handler: nil)
        }
        else{
            self.weatherListPresenter?.loadDaysWeather(predicate: self.predicate)
        }
        
    }
    
    func setDayWeaters(dayWeathers: [CDDayWeatherData]) {
        self.arrCDDayWeatherData = dayWeathers
        self.collectionView?.reloadData()
    }
}

extension WeatherListViewController: UIViewControllerTransitioningDelegate {
    func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return transition
    }
    
    func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return nil
    }
}
