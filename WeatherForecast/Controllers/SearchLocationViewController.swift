//
//  AddressListViewController.swift
//  WeatherForecast
//
//  Created by Afzaal Ahmad on 6/9/18.
//  Copyright © 2018 Afzaal Ahmad. All rights reserved.
//

import UIKit

protocol SearchLocationViewControllerDelegate{
    func didSelectAddress(address: Address, sender: SearchLocationViewController)
}

class SearchLocationViewController: UIViewController {

    @IBOutlet weak var emptyView: UIView?
    @IBOutlet weak var tableView: UITableView?

    public var delegate: SearchLocationViewControllerDelegate?
    
    let searchController = UISearchController(searchResultsController: nil)
    
    private let addressListPresenter = SearchAdressPresenter(addressService: AddressServiceAPI())
    
    private var addressesToDisplay = [Address]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView?.dataSource = self
        addressListPresenter.attachView(view: self)
        emptyView?.isHidden = true
        
        searchController.searchResultsUpdater = self
        searchController.obscuresBackgroundDuringPresentation = false
        
        searchController.searchBar.placeholder = "Search Location"
        navigationItem.searchController = searchController
    
        
        definesPresentationContext = true
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        searchController.isActive = true
    }
    
    @objc func searchAddesses(_ searchBar: UISearchBar) {
        guard let text = searchController.searchBar.text, text.count > 0 else{
            return
        }
        
        addressListPresenter.getAddresses(searchFor: text)
    }
}

extension SearchLocationViewController: UISearchResultsUpdating {
    // MARK: - UISearchResultsUpdating Delegate
    func updateSearchResults(for searchController: UISearchController) {
        
        ///Search addesses action will never be called more than once each 0.75 second interval
        NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(self.searchAddesses(_:)), object: searchController.searchBar)
        perform(#selector(self.searchAddesses(_:)), with: searchController.searchBar, afterDelay: 0.75)
    }
}

extension SearchLocationViewController: SearchAddressView{
    func startLoading() {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    func finishLoading() {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
    
    func setAddresss(addresses: [Address]) {
        addressesToDisplay = addresses
        tableView?.isHidden = false
        emptyView?.isHidden = true;
        tableView?.reloadData()
    }
    
    func setEmptyAddresses() {
        addressesToDisplay = []
        tableView?.isHidden = true
        emptyView?.isHidden = false;
        tableView?.reloadData()
    }
}


extension SearchLocationViewController: UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return addressesToDisplay.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Address-cell", for: indexPath)
        cell.textLabel?.text = self.addressesToDisplay[indexPath.row].formattedAddress
        
        return cell
    }
}

extension SearchLocationViewController: UITableViewDelegate{
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.delegate?.didSelectAddress(address: addressesToDisplay[indexPath.row], sender: self)
    }
}



