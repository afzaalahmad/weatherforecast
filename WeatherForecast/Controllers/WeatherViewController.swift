//
//  WeatherForcastViewController.swift
//  WeatherForecast
//
//  Created by Afzaal Ahmad on 6/12/18.
//  Copyright © 2018 Afzaal Ahmad. All rights reserved.
//

import UIKit

class WeatherViewController: UIViewController {

    var weatherListViewController: WeatherListViewController!
    
    var weatherPredicate: NSPredicate?
    var listOrderAscending = true
    
    override func viewDidLoad() {
        super.viewDidLoad()

        weatherListViewController.weatherPredicate = weatherPredicate
        weatherListViewController.listOrderAscending = listOrderAscending
    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let vc = segue.destination as? WeatherListViewController{
            self.weatherListViewController = vc
        }
        else if segue.destination.restorationIdentifier == "weather-history-navigation"{
            
            guard let navigationC = segue.destination as? UINavigationController, let weatherVC = navigationC.viewControllers.first as? WeatherViewController else{
                return
            }
            //Set forecast weather configration
            weatherVC.weatherPredicate = NSPredicate(format: "time < %@", Date().dateAtMidnight() as CVarArg)
            weatherVC.listOrderAscending = false
            
        }
        if let navigationVC = segue.destination as? UINavigationController{
            if let locationListVC = navigationVC.viewControllers.first as? LocationListViewController{
                locationListVC.delegate = self
            }
        }
    }
    
    @IBAction func unWindToWeatherViewController(segue: UIStoryboardSegue){}

    @IBAction func refreshData(_ sender: Any) {
        self.weatherListViewController.syncData()
    }
    
}

extension WeatherViewController: LocationListViewControllerDelegate{
    func didChangedLocation(cdLocation: CDLocation, sender: LocationListViewController) {
        self.weatherListViewController.syncData()
    }
}
