//
//  CDDayWeatherDetailViewController.swift
//  WeatherForecast
//
//  Created by Afzaal Ahmad on 6/11/18.
//  Copyright © 2018 Afzaal Ahmad. All rights reserved.
//

import UIKit

class DayWeatherDetailViewController: UIViewController {

    var dayWeatherData: CDDayWeatherData!
    
    @IBOutlet weak var iconView: SKYIconView!
    @IBOutlet weak var summaryLabel: UILabel!
    @IBOutlet weak var sunRiseLabel: UILabel!
    @IBOutlet weak var sunSetLabel: UILabel!
    @IBOutlet weak var hotTempLabel: UILabel!
    @IBOutlet weak var coldTempLabel: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.configureView()
    }
    
    func configureView() {
        self.title = dayWeatherData.formatedDate
        self.iconView.setType = dayWeatherData.skycons
        self.summaryLabel.text = dayWeatherData.summary
        self.sunRiseLabel.text = dayWeatherData.sunRiseTime
        self.sunSetLabel.text = dayWeatherData.sunSetTime
        self.hotTempLabel.text = dayWeatherData.tempHigh
        self.coldTempLabel.text = dayWeatherData.tempLow
    }
}
